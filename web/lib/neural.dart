import 'dart:math';
import 'dart:collection';

class Genome {
  Map<int, GenomeEntry> data = {};
  int _hiddenLayers = 0;
  int _index = 1;

  Genome({int inputs: 1, int outputs: 1}) {
    for (int i = 0; i < inputs; i++, _index++)
      data[_index] = new GenomeEntry(_index, 0, NodeType.INPUT, {});

    for (int i = 0; i < outputs; i++, _index++)
      data[_index] = new GenomeEntry(_index, 1, NodeType.OUTPUT, {});
  }

  Genome cross(Genome other, double weight, Random rng) {
    return null;
  }

  void mutate(MutationParameters parameters, Random rng) {
    Map<int, int> breakLinks = {};
    data.forEach((int nodeId, GenomeEntry entry) {
      entry.connections.forEach((int connectedTo, double weight) {
        if (rng.nextDouble() < parameters.mutateBias) {
          double rate = 1 - parameters.mutationStrength / 2 + parameters.mutationStrength * rng.nextDouble();
          entry.connections[connectedTo] = entry.connections[connectedTo] * rate * (rng.nextDouble() < parameters.newNeuronBias ? -1 : 1);
        }
        if (rng.nextDouble() < parameters.linkBreakBias) {
          breakLinks[nodeId] = connectedTo;
        }
      });
    });

    breakLinks.forEach((int node, int connectedTo) {
      data[node].connections.remove(connectedTo);
    });

    for (int i = 0; i < parameters.newLinkTests; i++) {
      if (rng.nextDouble() < parameters.newLinkBias) {
        List<int> neurons = data.keys.where((int id) {
          return data[id].type != NodeType.INPUT;
        }).toList();

        int id = neurons[rng.nextInt(neurons.length)];
        int depth = data[id].depth;

        List<int> possibleConnections = data.keys.where((int id) {
          return data[id].depth < depth && !data[id].connections.containsKey(id);
        }).toList();

        if (possibleConnections.length > 0) {
          int connectTo = possibleConnections[rng.nextInt(possibleConnections.length)];
          double weight = rng.nextDouble() * (rng.nextBool() ? -1 : 1) * parameters.maxInitialWeight;
          data[id].connections[connectTo] = weight;
        }
      }
    }

    if (rng.nextDouble() < parameters.newNeuronBias) {

      List<GenomeEntry> potential = data.values.where((GenomeEntry entry) => entry.connections.length >= 2).toList();
      if (potential.length == 0) return;
      GenomeEntry entry = potential[rng.nextInt(potential.length)];
      List<Pair<Connection, Connection>> pairs = _findConnectionPairs(entry);
      if (pairs.length == 0) return;
      Pair<Connection, Connection> pair = pairs[rng.nextInt(pairs.length)];
      GenomeEntry entry2 = data[pair.first.to];
      GenomeEntry entry3 = data[pair.second.to];

      int space = entry.depth - entry2.depth - 1;
      int insertId = null;
      if (space == 0) {
        insertId = entry.depth;
        data.values.where((GenomeEntry entry) => entry.depth >= insertId).forEach((GenomeEntry entry) {
          entry.depth++;
        });
      }
      else
        insertId = entry2.depth + (space == 1 ? 1 : rng.nextInt(space-1)+1);

      _index++;
      data[_index] = new GenomeEntry(_index, insertId, NodeType.HIDDEN, {
        entry2.nodeId: entry.connections[entry2.nodeId],
        entry3.nodeId: entry.connections[entry3.nodeId]
      });

      entry.connections.remove(entry2.nodeId);
      entry.connections.remove(entry3.nodeId);
      entry.connections[_index] = rng.nextDouble() * (rng.nextBool() ? -1 : 1) * parameters.maxInitialWeight;

    }
  }

  Genome.clone(Genome other) {
    _index = other._index;
    _hiddenLayers = other._hiddenLayers;
    other.data.forEach((K, V) {
      data[K] = new GenomeEntry.clone(V);
    });
  }

  List<Pair<Connection, Connection>> _findConnectionPairs(GenomeEntry entry) {
    List<Pair<Connection, Connection>> results = [];
    int nodeId = entry.nodeId;

    List<int> keys = entry.connections.keys.toList();
    for (int i = 0; i < keys.length-1; i++)
    {
      int depth1 = data[keys[i]].depth;
      for (int o = i + 1; o < keys.length; o++)
      {
        int depth2 = data[keys[o]].depth;

        if (depth1 == depth2)
        {
          Connection c1 = new Connection(nodeId, keys[i], entry.connections[keys[i]]);
          Connection c2 = new Connection(nodeId, keys[o], entry.connections[keys[o]]);
          results.add(new Pair(c1, c2));
        }
      }
    }

    return results;
  }
}

class Pair<K, V> {
  final K first;
  final V second;
  Pair(K this.first, V this.second);
}

class Connection {
  int from, to;
  double weight;

  Connection(int this.from, int this.to, double this.weight);

  Connection.clone(Connection other) {
    from = other.from;
    to = other.to;
    weight = other.weight;
  }
}

class GenomeEntry {
  int depth = 0;
  int nodeId = 0;
  NodeType type;
  Map<int, double> connections = {};

  GenomeEntry(int this.nodeId, int this.depth, NodeType this.type, Map<int, double> this.connections);

  GenomeEntry.clone(GenomeEntry other) {
    depth = other.depth;
    type = other.type;
    nodeId = other.nodeId;
    connections = new Map.from(other.connections);
  }
}

enum NodeType { INPUT, HIDDEN, OUTPUT }

class MutationParameters {

  final double newNeuronBias, newLinkBias, mutateBias,
    negativeBias, linkBreakBias, mutationStrength,
    maxInitialWeight;
  final int newLinkTests;

  MutationParameters({
    this.maxInitialWeight: 3.0,
    this.newNeuronBias: 0.5,
    this.newLinkTests: 3,
    this.newLinkBias: 0.2,
    this.mutateBias: 0.1,
    this.negativeBias: 0.1,
    this.linkBreakBias: 0.005,
    this.mutationStrength: 0.2
  });
}

class NeuronNetwork {
  Map<int, NeuronLayer> layers = {};

  List<Neuron> get inputs => layers[0].neurons;

  List<Neuron> get outputs => layers[layers.keys.last].neurons;

  NeuronNetwork(Genome genome) {
    Map<int, Neuron> neurons = {};
    genome.data.forEach((int neuronId, GenomeEntry entry) {
      neurons[neuronId] = new Neuron(neuronId);
      if (!layers.containsKey(entry.depth))
        layers[entry.depth] = new NeuronLayer(entry.depth);
      layers[entry.depth].addNeuron(neurons[neuronId]);
    });

    genome.data.forEach((int neuronId, GenomeEntry entry) {
      entry.connections.forEach((int connectionId, double weight) {
        neurons[neuronId].connections[neurons[connectionId]] = weight;
      });
    });
  }

  void update() {
    List<int> keys = layers.keys.toList()..sort();
    for (int key in keys)
      if (key != 0)
        layers[key].neurons.forEach((Neuron neuron) {
          neuron.updateValue();
        });
  }
}

class NeuronLayer {
  int depth;
  List<Neuron> _neurons = [];

  NeuronLayer(int this.depth);

  void addNeuron(Neuron neuron) => _neurons.add(neuron);

  get neurons => _neurons;
}

class Neuron {
  int id;
  NeuronLayer layer;
  double value = 0.0;

  Map<Neuron, double> connections = {};

  Neuron(int this.id);

  updateValue() {
    value = 0.0;
    connections.forEach((Neuron neuron, double weight) {
      value += neuron.value * weight;
    });

    value = (pow(E, value) - pow(E, -value))/(pow(E, value) + pow(E, -value)); // tanh function
  }
}
