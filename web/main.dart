// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';
import 'dart:math';
import 'lib/neural.dart';

NeuronNetwork network;
CanvasElement canvas;

void main() {
  canvas = querySelector('#canvas');

  Genome genome = new Genome(inputs: 5, outputs: 3);
  MutationParameters parameters = new MutationParameters(newLinkTests: 3);
  Random random = new Random();

  for (int i = 0; i < 20; i++)
    genome.mutate(parameters, random);

  network = new NeuronNetwork(genome);



  window.animationFrame.then(renderLoop);
}

num prevTime = 0;
double x = 0;

void renderLoop(num time) {
  num delta = (time - prevTime) / 1000;
  prevTime = time;

  x += PI*2*delta*0.5;

  List<Neuron> inputs = network.inputs;
  inputs[0].value = sin(x);
  inputs[1].value = sin(x + PI*2/10);
  inputs[2].value = sin(x + PI*2/10*2);
  inputs[3].value = sin(x + PI*2/10*3);
  inputs[4].value = sin(x + PI*2/10*4);
  network.update();

  renderNetwork(network, delta);

  window.animationFrame.then(renderLoop);
}

void renderNetwork(NeuronNetwork network, num delta) {
  CanvasRenderingContext2D context = canvas.context2D;

  int height = canvas.height;
  int offsetHorizontal = 50;
  int offsetVertical = 50;

  Map<Neuron, Point> positions = {};
  int l = 0;
  network.layers.forEach((int index, NeuronLayer layer) {
    int i = 0;
    l++;
    int space = offsetVertical * (layer.neurons.length - 1);
    layer.neurons.forEach((Neuron neuron) {
      int offset = 5*(1+l%3);
      positions[neuron] = new Point(offsetHorizontal * (index + 1), height / 2 - space / 2 + offsetVertical * i + offset);
      i++;
    });
  });

  context
    ..fillStyle = "white"
    ..fillRect(0, 0, canvas.width, canvas.height);

  List<int> keys = network.layers.keys.toList() ..sort();
  keys.reversed.forEach((int layerId) {
    NeuronLayer layer = network.layers[layerId];
    layer.neurons.forEach((Neuron neuron) {
      Point origin = positions[neuron];

      neuron.connections.keys.forEach((Neuron targetNeuron) {
        Point target = positions[targetNeuron];
        double weight = neuron.connections[targetNeuron];
        context
          ..lineWidth = weight
          ..strokeStyle = weight > 0 ? "green" : "red"
          ..globalAlpha = targetNeuron.value.abs()
          ..beginPath()
          ..moveTo(origin.x, origin.y)
          ..lineTo(target.x, target.y)
          ..stroke();
      });


      context
        ..globalAlpha = 1.0
        ..fillStyle = "white"
        ..beginPath()
        ..arc(origin.x, origin.y, 10, 0, PI * 2)
        ..fill()
        ..lineWidth = 2
        ..beginPath()
        ..strokeStyle = "black"
        ..arc(origin.x, origin.y, 11, 0, PI * 2)
        ..stroke()
        ..fillStyle = neuron.value > 0 ? "green" : "red"
        ..globalAlpha = 0.7
        ..beginPath()
        ..arc(origin.x, origin.y, (10 * neuron.value).abs(), 0, PI * 2)
        ..fill()
        ..fillStyle = "black"
        ..textAlign = "center"
        ..fillText("${neuron.id}", origin.x, origin.y + 3);
    });
  });

  context
    ..fillStyle = "black"
    ..textAlign = "left"
    ..fillText("FPS: ${(1/delta).round()}", 2, 10);
}